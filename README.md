# crud_sql

### Topics

* [Project description](#Project-description)

* [Pre-requirements](#Pre-requirements)

* [Technologies used](#Technologies-used)

## Project description

<p align="justify">
Project create contacts
</p>

## Pre-requirements
### <b>1.0 Clone the GitLabs repository</b>
```
git clone git@gitlab.com:M_tardelly/crud_sql.git
```
### <b>2.0 Configure your database mySQL with ``application.properties``</b>
```
spring.datasource.url=jdbc:mysql://localhost:3306/user
spring.datasource.username=root
spring.datasource.password=Teste@123
```

### <b>3.0 Start application with Java 11 </b>
## Technologies used

* <b>Java 11</b>
* <b>Spring Boot</b>
* <b>MySQL</b>

## Developer

##### FullStack
[<img src="https://i1.wp.com/www.actionlabs.com.br/wp-content/uploads/2022/04/matheus-tardelly-actio-labs.jpg?resize=150%2C150&ssl=1" width=80><br><sub>Tardelly Santos</sub>](https://www.linkedin.com/in/tardelly-santos-93b499204/)  
