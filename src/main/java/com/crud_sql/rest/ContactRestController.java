package com.crud_sql.rest;

import com.crud_sql.exception.ResourceNotFoundException;
import com.crud_sql.helper.ContactHelper;
import com.crud_sql.helper.CreateContactHelper;
import com.crud_sql.helper.status.DefaultReponseHelper;
import com.crud_sql.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/contact")
@RequiredArgsConstructor
public class ContactRestController {

    private final ContactService service;

    @PostMapping
    public ResponseEntity<DefaultReponseHelper<ContactHelper>> create(@Valid @RequestBody CreateContactHelper objDto ) throws Exception {
            return ResponseEntity.ok().body(service.create(objDto));
    }
    @GetMapping("/{email}")
    public ResponseEntity<DefaultReponseHelper<ContactHelper>> find(@PathVariable String email) throws ResourceNotFoundException {
        return ResponseEntity.ok().body(service.find(email));
    }
    @DeleteMapping("/{email}")
    public ResponseEntity<DefaultReponseHelper<ContactHelper>> delete(@PathVariable String email) throws ResourceNotFoundException {
        return ResponseEntity.ok().body(service.delete(email));
    }
    @PutMapping("/{email}")
    public ResponseEntity<DefaultReponseHelper<ContactHelper>> update(@RequestBody CreateContactHelper objDto, @PathVariable String email) throws Exception {
        return ResponseEntity.ok().body(service.update(objDto, email));
    }
}
