package com.crud_sql.exception;

import lombok.Data;


@Data
public class ErrorDetails {

    boolean success;
    private String timestamp;
    private String message;
    private String details;

    public ErrorDetails(boolean success,String timestamp, String message, String details) {
        super();
        this.success = success;
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }
}
