package com.crud_sql.exception;

public class ResourceFoundException extends Exception{
    public ResourceFoundException(String message) {
        super(message);
    }
}
