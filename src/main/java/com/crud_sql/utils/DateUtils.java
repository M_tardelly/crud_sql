package com.crud_sql.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static Date getDateFromString(String date)
    {
        SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd");

        try
        {
            return tf.parse(date);
        }
        catch (Exception e)
        {
            //
        }

        return null;
    }

    public static Date getDateTimeFromString(String date)
    {
        SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        try
        {
            return tf.parse(date);
        }
        catch (Exception e)
        {
            //
        }

        return null;
    }

    public static String getDateTime(Date date)
    {
        SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        try
        {
            return tf.format(date);
        }
        catch (Exception e)
        {
            //
        }

        return null;
    }
    public static Date getFinalHoursDate(Date date)
    {
        SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd");

        try
        {
            String dateSum = tf.format(date)+ " 23:59";
            SimpleDateFormat tf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            return tf2.parse(dateSum);
        }
        catch (Exception e)
        {
            //
        }

        return null;
    }
    public static Date getInitialHoursDate(Date date)
    {
        SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = tf.format(date);
        try
        {
            return tf.parse(date1);
        }
        catch (Exception e)
        {
            //
        }

        return null;
    }



}
