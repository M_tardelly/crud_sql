package com.crud_sql.helper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
public class ApiCepHelper implements Serializable {

    private String status;

    private String code;

    private String state;

    private String city;

    private String address;
}
