package com.crud_sql.helper;

import com.crud_sql.model.Contact;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.NotFound;

import javax.persistence.Column;
import java.util.Date;

@Data
@AllArgsConstructor
public class ContactHelper {

    private Long id;

    private String email;

    private String name;

    private String phone;

    private String cep;

    private String address;

    private String city;

    private String uf;

    private Date createdAt;

    public ContactHelper(Contact obj) {
        this.id = obj.getId();
        this.email = obj.getEmail();
        this.name = obj.getName();
        this.phone = obj.getPhone();
        this.cep = obj.getCep();
        this.address = obj.getCep();
        this.city = obj.getCity();
        this.uf = obj.getUf();
        this.createdAt = obj.getCreatedAt();
    }
}
