package com.crud_sql.helper;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateContactHelper {

    @Email
    @NotEmpty(message = "O Campo email é obrigatorio")
    private String email;
    @NotEmpty(message = "O Campo name é obrigatorio")
    private String name;
    @NotEmpty(message = "O Campo phone é obrigatorio")
    private String phone;
    @NotEmpty(message = "O Campo cep é obrigatorio")

    private String cep;

    public CreateContactHelper(String email) {
        this.email = email;
    }
}
