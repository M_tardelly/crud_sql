package com.crud_sql.helper.status;


public class DefaultReponseHelper<T> extends DefaultStatusMessage {

    private T data;

    public DefaultReponseHelper(Boolean success) {
        super(success);
    }

    public DefaultReponseHelper(Boolean success, String message) {
        super(success, message);
    }

    public DefaultReponseHelper(Boolean success, String message, T data) {
        super(success, message);
        this.data = data;
    }


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
