package com.crud_sql.helper.status;



public class DefaultStatusMessage extends DefaultStatus{

    private String message;

    public DefaultStatusMessage(Boolean success) {
        this.success = success;
    }


    public DefaultStatusMessage(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
