package com.crud_sql.helper.status;


public class DefaultStatus {

    boolean success;

    public DefaultStatus(){

    }
    public DefaultStatus(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
