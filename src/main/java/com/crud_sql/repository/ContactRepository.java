package com.crud_sql.repository;

import com.crud_sql.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {

    boolean existsByEmail(String email);
    Optional<Contact> findByEmail(String email);
    void removeByEmail(String email);
}
