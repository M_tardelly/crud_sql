package com.crud_sql.model;

import com.crud_sql.helper.ContactHelper;
import com.crud_sql.helper.CreateContactHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@Table(name ="tb_contato")
@NoArgsConstructor
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    private String name;

    private String phone;

    private String cep;

    private String address;

    private String city;

    private String uf;

    private Date createdAt;

    public Contact(CreateContactHelper obj) {
        this.email = obj.getEmail();
        this.name = obj.getName();
        this.phone = obj.getPhone();
        this.cep = obj.getCep();
    }

    public Contact(ContactHelper obj) {
        this.id = obj.getId();
        this.email = obj.getEmail();
        this.name = obj.getName();
        this.phone = obj.getPhone();
        this.cep = obj.getCep();
        this.address = obj.getAddress();
        this.city = obj.getCity();
        this.uf = obj.getUf();
        this.createdAt = obj.getCreatedAt();
    }
    public void setAddresByCep(String address, String city, String uf){
        this.address = address;
        this.city = city;
        this.uf = uf;
    }
}
