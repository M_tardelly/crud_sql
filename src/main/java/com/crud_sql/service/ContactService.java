package com.crud_sql.service;

import com.crud_sql.exception.ResourceFoundException;
import com.crud_sql.exception.ResourceNotFoundException;
import com.crud_sql.helper.ApiCepHelper;
import com.crud_sql.helper.ContactHelper;
import com.crud_sql.helper.CreateContactHelper;
import com.crud_sql.model.Contact;
import com.crud_sql.repository.ContactRepository;
import com.crud_sql.helper.status.DefaultReponseHelper;
import com.crud_sql.utils.ParseUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class ContactService {

    private final ContactRepository repository;

    private final ApiCepService apiCepService;

    public DefaultReponseHelper<ContactHelper> create(CreateContactHelper objDto) throws Exception {
        Contact obj = fromDTO(objDto);

        if(repository.existsByEmail(obj.getEmail())){
            throw new ResourceFoundException("Email : "+obj.getEmail()+", já cadastrado .");
        }

        DefaultReponseHelper<ApiCepHelper> api = apiCepService.findCep(objDto.getCep());
        obj.setAddresByCep(api.getData().getAddress(),api.getData().getCity(),api.getData().getState());
        obj.setCreatedAt(new Date());
/**/        obj = repository.save(obj);

        return new DefaultReponseHelper<>(true,"Contato cadastrado com sucesso!.", new ContactHelper(obj));
    }

    public DefaultReponseHelper<ContactHelper> find(String email) throws ResourceNotFoundException {
        Optional<Contact> obj = repository.findByEmail(email);

        return obj.map(contact -> new DefaultReponseHelper<>(true, "Contato encontrado", new ContactHelper(contact)))
                .orElseThrow(() -> new ResourceNotFoundException("Contato com email : "+ email+ ", não econtrado" ));
    }

    public DefaultReponseHelper<ContactHelper> delete(String email) throws ResourceNotFoundException {
        if(!repository.existsByEmail(email)){
            throw new ResourceNotFoundException("Contato com email : "+ email+ ", não econtrado" );
        }

        repository.removeByEmail(email);
        return new DefaultReponseHelper<>(true,"Email : "+ email +", deletado com sucesso!.");
    }

    public DefaultReponseHelper<ContactHelper> update(CreateContactHelper objDto, String email) throws Exception {

        ContactHelper helper = find(email).getData();

        if(!objDto.getCep().equals(helper.getCep())){
            DefaultReponseHelper<ApiCepHelper> api = apiCepService.findCep(objDto.getCep());
            helper.setCity(api.getData().getCity());
            helper.setAddress(api.getData().getAddress());
            helper.setUf(api.getData().getState());
        }

        Contact update = fromDTO(helper);

        String[] nullProperties = ParseUtils.getBlankPropertyNames(objDto);
        BeanUtils.copyProperties(objDto, update, nullProperties);

        update = repository.save(update);

        return new DefaultReponseHelper<>(true, "Contato Atualizado com sucesso!.",
                new ContactHelper(update));
    }

    private Contact fromDTO(CreateContactHelper objDto){
        return new Contact(objDto);
    }
    private Contact fromDTO(ContactHelper objDto){
        return new Contact(objDto);
    }
}
