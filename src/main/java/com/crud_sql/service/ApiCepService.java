package com.crud_sql.service;

import com.crud_sql.exception.ResourceNotFoundException;
import com.crud_sql.helper.ApiCepHelper;
import com.crud_sql.helper.status.DefaultReponseHelper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiCepService {
    private static final String URI = "https://cdn.apicep.com/file/apicep/";

    public DefaultReponseHelper<ApiCepHelper> findCep(String cep) throws Exception {
        StringBuilder sb = new StringBuilder(cep);
        sb.insert(5,"-");
        sb.append(".json");
        RestTemplate rest = new RestTemplate();

        try {
            ResponseEntity<ApiCepHelper> response = rest.getForEntity(URI + sb, ApiCepHelper.class);
            return new DefaultReponseHelper<>(true,"CEP encontrado com sucesso", response.getBody());
        }catch (Exception e){
            throw new ResourceNotFoundException("CEP:"+ cep + ", não encontrado ");
        }

    }
}
