package com.crud_sql.service;

import com.crud_sql.exception.ResourceFoundException;
import com.crud_sql.exception.ResourceNotFoundException;
import com.crud_sql.helper.ContactHelper;
import com.crud_sql.helper.CreateContactHelper;
import com.crud_sql.helper.status.DefaultReponseHelper;
import com.crud_sql.model.Contact;
import com.crud_sql.repository.ContactRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ContactServiceTest {

    private ContactService contactService;

    @Mock
    private ApiCepService apiCepService;

    @Mock
    private ContactRepository contactRepository;

    @Captor
    private ArgumentCaptor<Contact> captor;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        this.contactService = new ContactService(contactRepository, apiCepService);
    }


    @Test
    public void deleteUserExists() throws Exception {
        when(contactRepository.existsByEmail("matheustardelly@gmail.com")).thenReturn(true);
        contactService.delete("matheustardelly@gmail.com");
        verify(contactRepository, times(1)).removeByEmail("matheustardelly@gmail.com");
    }

    @Test(expected = ResourceNotFoundException.class)
    public void deleteUserNotExists() throws ResourceNotFoundException {
        when(contactRepository.existsByEmail("matheustardelly@gmail.com")).thenReturn(false);
        contactService.delete("matheustardelly@gmail.com");
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findUserNotExists() throws Exception {
        Optional<Contact> obj = Optional.empty();
        when(contactRepository.findByEmail("matheustardelly@gmail.com")).thenReturn(obj);
        contactService.find("matheustardelly@gmail.com");
    }

    @Test
    public void findUserExists() throws Exception {
        Optional<Contact> obj = Optional.of(new Contact(123L,"matheustardelly@gmail.com","matheus","83987939768","58406840","Rua Isabel Alexandre Bernadino","Campina Grande","PB",new Date()));
        when(contactRepository.findByEmail("matheustardelly@gmail.com")).thenReturn(obj);
        DefaultReponseHelper<ContactHelper> defaultReponseHelper = contactService.find("matheustardelly@gmail.com");
        assertTrue(defaultReponseHelper.isSuccess());
        assertEquals(defaultReponseHelper.getData().getEmail(),"matheustardelly@gmail.com");
        verify(contactRepository, times(1)).findByEmail("matheustardelly@gmail.com");
    }

    @Test(expected = ResourceFoundException.class)
    public void createUserExists() throws Exception {
        CreateContactHelper contactHelper = new CreateContactHelper("matheustardelly@gmail.com","matheus","83987939768","58406840");
        when(contactRepository.existsByEmail(contactHelper.getEmail())).thenReturn(true);
        contactService.create(contactHelper);
        verify(contactRepository, times(1)).findByEmail(contactHelper.getEmail());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void updateUserNotExists() throws Exception {
        CreateContactHelper contactHelper = new CreateContactHelper("matheustardelly@gmail.com","matheus","83987939768","58406840");
        when(contactRepository.existsByEmail(contactHelper.getEmail())).thenReturn(false);
        contactService.update(contactHelper, contactHelper.getEmail());
        verify(contactRepository, times(1)).findByEmail(contactHelper.getEmail());
    }
}
