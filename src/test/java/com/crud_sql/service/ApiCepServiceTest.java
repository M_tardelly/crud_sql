package com.crud_sql.service;

import com.crud_sql.exception.ResourceNotFoundException;
import com.crud_sql.helper.ApiCepHelper;
import com.crud_sql.helper.status.DefaultReponseHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class ApiCepServiceTest {
    private ApiCepService apiCepService;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        this.apiCepService = new ApiCepService();
    }

    @Test
    public void findCep() throws Exception {
        ApiCepHelper apiCepHelper = new ApiCepHelper();
        apiCepHelper.setCode("58406-840");
        apiCepHelper.setStatus("200");
        apiCepHelper.setCity("Campina Grande");
        apiCepHelper.setAddress("Rua Isabel Alexandre Bernadino");
        apiCepHelper.setState("PB");

        DefaultReponseHelper<ApiCepHelper> defaultReponseHelper =  apiCepService.findCep("58406840");

        Assert.assertEquals(apiCepHelper.getCity(),defaultReponseHelper.getData().getCity());
        Assert.assertEquals(apiCepHelper.getState(),defaultReponseHelper.getData().getState());
        Assert.assertEquals(apiCepHelper.getAddress(),defaultReponseHelper.getData().getAddress());
        Assert.assertEquals(apiCepHelper.getCode(),defaultReponseHelper.getData().getCode());
        Assert.assertEquals(apiCepHelper.getCode(),"58406-840");
        Assert.assertTrue(defaultReponseHelper.isSuccess());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findCepNotExist() throws Exception {

        DefaultReponseHelper<ApiCepHelper> defaultReponseHelper =  apiCepService.findCep("52306840");

        Assert.assertTrue(defaultReponseHelper.isSuccess());
    }
}
